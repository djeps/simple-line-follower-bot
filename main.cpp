#include "mbed.h"

#define I2C_SDA P20
#define I2C_SCL P19

const int MOTOR_ADDRESS = 0x10 << 1;

const char MOTOR_L = 0x00;
const char MOTOR_R = 0x02;

const char DIR_CW = 0x00;
const char DIR_CCW = 0x01;

const char TURN_L = 0x00;
const char TURN_R = 0x01;

const char PIVOT_L = 0x00;
const char PIVOT_R = 0x01;

void botStop(I2C _i2c, const int _address);
void botMove(I2C _i2c, const int _address, char _direction, char _speed);
void botMoveForwards(I2C _i2c, const int _address, char _speed);
void botMoveBackwards(I2C _i2c, const int _address, char _speed);
void botTurn(I2C _i2c, const int _address, char _direction, char _speed);
void botTurnLeft(I2C _i2c, const int _address, char _speed);
void botTurnRight(I2C _i2c, const int _address, char _speed);
void botPivot(I2C _i2c, const int _address, char _direction, char _speed);
void botPivotLeft(I2C _i2c, const int _address, char _speed);
void botPivotRight(I2C _i2c, const int _address, char _speed);

I2C i2c(I2C_SDA, I2C_SCL);

DigitalIn LINE_SENS_L(P13);
DigitalIn LINE_SENS_R(P14);

DigitalOut LED_L(P8);
DigitalOut LED_R(P12);

// TODO:
// BLE support and a companion Android app

int main(void)
{    
    LINE_SENS_L.mode(PullNone);
    LINE_SENS_R.mode(PullNone);
    
    i2c.frequency(200000);
    
    char speed_100pct = 0xFF; // Full Speed 100%
    char speed_050pct = 0x7F; // Half Speed 50%
    char speed_025pct = 0x3F;  // Quarter Speed 25%
    
    LED_L = !(LINE_SENS_L);
    LED_R = !(LINE_SENS_R);
    
    wait_ms(3000);
    
    while(1)
    {
        LED_L = !(LINE_SENS_L);
        LED_R = !(LINE_SENS_R);
        
        if (LED_L && LED_R)
        {
            botMoveForwards(i2c, MOTOR_ADDRESS, speed_050pct);
        }
        else
        {
            if (LED_L && !LED_R)
            {
                botTurnLeft(i2c, MOTOR_ADDRESS, speed_025pct);
            }
            else if (!LED_L && LED_R)
            {
                botTurnRight(i2c, MOTOR_ADDRESS, speed_025pct);
            }
            else
            {
                botMoveBackwards(i2c, MOTOR_ADDRESS, speed_025pct);
            }
        }
    }
}

void botStop(I2C _i2c, const int _address)
{
    char buff[3] = {MOTOR_L, DIR_CW, 0x00};
    int buffSize = sizeof(buff) / sizeof(char);
    
    _i2c.write(_address, buff, buffSize);
    
    buff[0] = MOTOR_R;
    _i2c.write(_address, buff, buffSize);
}

void botMove(I2C _i2c, const int _address, char _direction, char _speed)
{
    char buff[3] = {MOTOR_L, DIR_CW, 0x00};
    int buffSize = sizeof(buff) / sizeof(char);
    
    buff[1] = _direction;
    buff[2] = _speed;
    
    _i2c.write(_address, buff, buffSize);
    
    buff[0] = MOTOR_R;
    _i2c.write(_address, buff, buffSize);
}

void botMoveForwards(I2C _i2c, const int _address, char _speed)
{
    botMove(_i2c, _address, DIR_CW, _speed);
}

void botMoveBackwards(I2C _i2c, const int _address, char _speed)
{
    botMove(_i2c, _address, DIR_CCW, _speed);
}

void botTurn(I2C _i2c, const int _address, char _direction, char _speed)
{
    botStop(_i2c, _address);
    
    char buff[3] = {0x00, DIR_CW, 0x00};
    int buffSize = sizeof(buff) / sizeof(char);
    
    switch(_direction)
    {
        case TURN_L:
            buff[0] = MOTOR_R;
            break;
            
        case TURN_R:
        default:
            buff[0] = MOTOR_L;
            break;
    }
    
    buff[2] = _speed;
    
    _i2c.write(_address, buff, buffSize);
}

void botTurnLeft(I2C _i2c, const int _address, char _speed)
{
    botTurn(_i2c, _address, TURN_L, _speed);
}

void botTurnRight(I2C _i2c, const int _address, char _speed)
{
    botTurn(_i2c, _address, TURN_R, _speed);
}

void botPivot(I2C _i2c, const int _address, char _direction, char _speed)
{
    botStop(_i2c, _address);
    
    char buff[3] = {0x00, 0x00, 0x00};
    int buffSize = sizeof(buff) / sizeof(char);
    
    buff[2] = _speed;
    
    switch(_direction)
    {
        case PIVOT_L:
            buff[0] = MOTOR_R;
            buff[1] = DIR_CW;
            _i2c.write(_address, buff, buffSize);
            
            buff[0] = MOTOR_L;
            buff[1] = DIR_CCW;
            _i2c.write(_address, buff, buffSize);
            break;
            
        case PIVOT_R:
        default:
            buff[0] = MOTOR_L;
            buff[1] = DIR_CW;
            _i2c.write(_address, buff, buffSize);
            
            buff[0] = MOTOR_R;
            buff[1] = DIR_CCW;
            _i2c.write(_address, buff, buffSize);
            break;
    }
}

void botPivotLeft(I2C _i2c, const int _address, char _speed)
{
    botPivot(_i2c, _address, PIVOT_L, _speed);
}

void botPivotRight(I2C _i2c, const int _address, char _speed)
{
    botPivot(_i2c, _address, PIVOT_R, _speed);
}
